from django.conf.urls import patterns, include, url

from django.views.generic import TemplateView
from django.contrib import admin

from sms.views import SmsView
admin.autodiscover()


urlpatterns = patterns('',
                       url(r'^$', SmsView.as_view(), name='home'),
                       url(r'success/$',
                           TemplateView.as_view(template_name='success.html'), name='success'),

                       url(r'^admin/', include(admin.site.urls)),
                       )
