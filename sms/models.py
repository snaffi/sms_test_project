# -*- coding: utf-8 -*-

from django.db import models


class Sms(models.Model):
    SMS_STATUS_CHOICES = (
        (0, u'Отправлено'),
        (1, u'Доставлено'),
        (2, u'Ошибка'),
    )
    phone = models.CharField(u'Телефон', max_length=20)
    text = models.CharField(u'Текст', max_length=200)
    status = models.SmallIntegerField(
        u'Статус', choices=SMS_STATUS_CHOICES, default=0)
    api_uri = models.URLField(u'URI смс провайдера', max_length=100)
    error_message = models.TextField(u'Описание ошибки', null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.phone + "  " + self.text

    @classmethod
    def create(cls, **kwargs):
        sms = cls(**kwargs)
        sms.status = 0
        sms.save()
        return sms

    def delivered(self):
        self.status = 1
        self.save()

    def error(self, error_message=None):
        print 111111
        self.error_message = error_message
        self.status = 2
        self.save()
