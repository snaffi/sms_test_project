# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sms',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone', models.CharField(max_length=20, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('text', models.CharField(max_length=200, verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('status', models.SmallIntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u043e'), (1, '\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d\u043e'), (2, '\u041e\u0448\u0438\u0431\u043a\u0430')])),
                ('api_uri', models.URLField(max_length=100, verbose_name='URI \u0441\u043c\u0441 \u043f\u0440\u043e\u0432\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('error_message', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043e\u0448\u0438\u0431\u043a\u0438', blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
