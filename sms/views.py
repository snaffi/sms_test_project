# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect

from django.views.generic import View
from smstransport import smstransport, smstransports


class SmsView(View):
    template_name = "sms.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        phone = request.POST['phone']
        text = request.POST['text']

        print("Send default transport")
        smstransport.send(phone, text)

        print("Send custom transport")
        smstransports['custom'].send(phone, text)

        print("Send sms traffic")
        smstransports['sms_traffic'].send(phone, text)
        return redirect('success')
