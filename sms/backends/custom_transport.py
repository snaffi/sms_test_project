# -*- coding: utf-8 -*-

import requests

from django.core.exceptions import ImproperlyConfigured

from smstransport.backends.dummy_backend import BaseSmsTrasportBackend
from sms.models import Sms


class CustomSmsTransport(BaseSmsTrasportBackend):

    def send(self, phone, text):
        print("Yeah, i am custom sms transport")
        print(phone, text)
        print("My params")
        print(self.params)


class SmsTrafficTransport(BaseSmsTrasportBackend):

    def __init__(self, **kwargs):
        super(SmsTrafficTransport, self).__init__(**kwargs)
        print 123332432423
        if 'api_uri' not in self.params:
            raise ImproperlyConfigured('api_uri is None')

    def send(self, phone, text):
        payload = {'phone': phone, 'text': text}
        sms = Sms.create(**payload)
        sms.api_uri = self.params['api_uri']
        sms.save()
        r = requests.post(self.params['api_uri'], params=payload)
        if r.status_code == 200:
            response = r.json()
            if response['status'] == 'ok':
                sms.delivered()
                return True
            else:
                print(response['error_code'], response['error_msg'])
                sms.error(response['error_msg'])
                return False
        print("HTTP status", r.status_code)
        sms.error("HTTP status " + str(r.status_code))
        return False
